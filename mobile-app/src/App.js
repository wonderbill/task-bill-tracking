import React from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
} from 'react-native';
import {NavigationContainer, useNavigation} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {Provider, useDispatch, useSelector} from 'react-redux';
import store from './state/store';
import {createPayment} from './state/payments';

const App = () => (
  <Provider store={store}>
    <NavigationContainer>
      <Navigator />
    </NavigationContainer>
  </Provider>
);

const Stack = createStackNavigator();
const Navigator = () => (
  <Stack.Navigator>
    <Stack.Screen name="home" component={Home} />
    <Stack.Screen name="example" component={Example} />
  </Stack.Navigator>
);

const Home = () => {
  const navigation = useNavigation();

  return (
    <SafeAreaView>
      <StatusBar barStyle={'dark-content'} />
      <TouchableOpacity
        style={styles.button}
        onPress={() => navigation.navigate('example')}>
        <Text style={styles.buttonText}>Go to "example screen"</Text>
      </TouchableOpacity>
    </SafeAreaView>
  );
};

const Example = () => {
  const dispatch = useDispatch();
  const payments = useSelector(s => s.payments);

  const exampleAddPayment = () =>
    dispatch(
      createPayment({
        name: 'Hello World',
      }),
    );

  return (
    <SafeAreaView>
      <StatusBar barStyle="dark-content" />
      <ScrollView contentInsetAdjustmentBehavior="automatic">
        <Text style={styles.heading}>Example</Text>

        {payments.map((payment, index) => (
          <Text key={index}>{payment.name}</Text>
        ))}
        <TouchableOpacity style={styles.button} onPress={exampleAddPayment}>
          <Text style={styles.buttonText}>Add Payment</Text>
        </TouchableOpacity>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  heading: {
    fontSize: 32,
    fontWeight: 'bold',
    marginBottom: 16,
  },
  button: {
    backgroundColor: 'green',
    alignItems: 'center',
    borderRadius: 16,
  },
  buttonText: {
    fontSize: 16,
    fontWeight: '500',
    color: 'white',
    padding: 16,
  },
});

export default App;
